let collection = [];
//let count = 0;

// Write the queue functions below.

print = () => {
    return collection
};

enqueue = (name) => {
    if(collection.length !== 0){
        collection[collection.length] = name;
    } else {
        collection[0] = name;
    }
    return collection;
};

dequeue = () => {
    let newArr = [];
    for(let index = 0; index < collection.length - 1; index++){
        newArr[index] = collection[++index];
    }
    return collection = newArr;
};

front = () => {
    return collection[0];
}

size = () => {
    return collection.length;
}

isEmpty = () => {
    if (collection.length > 0) {
        return false
    } else {
        return true
    }
}

module.exports = { 
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty  
};